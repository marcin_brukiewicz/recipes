import axios from 'axios';

const create = () => {
  const baseURL = '/api';
  const api = axios.create({
    baseURL,
  });

  const getRecipe = (query, page) =>
    api.get('/', { params: { i: query, p: page } });

  return {
    getRecipe,
  };
};

export default {
  create,
};
