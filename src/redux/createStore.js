import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { middleware as reduxPackMiddleware } from 'redux-pack';
import logger from 'redux-logger';
import rootReducer from 'Reducers';
import API from '../services/api';

export default (history) => {
  const api = API.create();
  const middlewares = [
    thunk.withExtraArgument({ api, history }),
    reduxPackMiddleware,
  ];
  // if (process.env.NODE_ENV !== 'production') {
  //   middlewares.push(logger);
  // }
  return createStore(
    rootReducer,
    applyMiddleware(...middlewares),
  );
};
