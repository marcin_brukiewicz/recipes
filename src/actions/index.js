import * as types from './types';

export const loadRecipes = (query, page = 1) => (dispatch, getState, { api, history }) => {
  dispatch({
    type: types.SET_QUERY,
    payload: query,
  });
  return dispatch({
    type: types.LOAD_RECIPES,
    promise: api.getRecipe(query, page),
    meta: {
      page,
    },
  }).then(response => {
    if (response.payload.status === 200)
      history.push(`/?q=${query}&page=${page}`)
  });
}

export const changePage = (page) => (dispatch) =>
  dispatch({
    type: types.CHANGE_PAGE,
    payload: page,
  });
