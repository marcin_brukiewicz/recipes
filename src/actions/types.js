export const LOAD_RECIPES = 'LOAD_RECIPES';
export const CHANGE_PAGE = 'CHANGE_PAGE';
export const SET_QUERY = 'SET_QUERY';
