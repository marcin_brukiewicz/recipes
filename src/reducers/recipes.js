import { handle } from 'redux-pack';
import { LOAD_RECIPES, CHANGE_PAGE, SET_QUERY } from 'Actions/types';
import R from 'ramda';

const initialState = {
  loading: false,
  error: null,
  results: {},
  currentPage: null,
  query: null,
};

export default (state = initialState, action) => {
  const { payload, meta } = action;
  switch (action.type) {
    case SET_QUERY:
      return { ...state, query: payload };
    case CHANGE_PAGE:
      return { ...state, currentPage: payload };
    case LOAD_RECIPES:
      return handle(state, action, {
        start: prevState => ({
          ...prevState,
          loading: true,
          error: null,
        }),
        finish: prevState => ({ ...prevState, loading: false }),
        failure: prevState => ({ ...prevState, error: payload.message }),
        success: (prevState) => {
          const results = { ...prevState.results };
          results[meta.page] = R.sortWith([
            R.ascend(R.prop('title')),
          ])(payload.data.results)
          return {
            ...prevState,
            results,
            currentPage: meta.page,
          };
        },
      });
    default:
      return state;
  }
};
