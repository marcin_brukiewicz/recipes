import React from 'react';
import queryString from 'query-string'
import { connect } from 'react-redux';
import { compose, withHandlers, lifecycle } from 'recompose';
import * as actions from 'Actions';
import SearchBox from './recipes/SearchBox';
import Results from './recipes/Results';

const Recipes = ({
  search,
  onPageChange,
  changeQuery,
  recipes: { query, currentPage, loading, results}
}) => (
  <div className="container">
    <div className="box">
      <SearchBox search={search} query={query}/>
      <Results
        changePage={onPageChange}
        results={results[currentPage] || []}
        currentPage={currentPage}
        changeQuery={changeQuery}
        loading={loading}
        query={query}
      />
    </div>
  </div>
)

const withLifecycle = lifecycle({
  componentWillMount: function() {
    this.props.update();
  },
});

const handlers = withHandlers({
  update: ({ history, loadRecipes, recipes }) => () => {
    const query = queryString.parse(history.location.search);
    if (query.page && query.q && recipes.query != query.q) {
      loadRecipes(query.q, parseInt(query.page));
    }
  },
  search: ({ loadRecipes }) => (query) => loadRecipes(query),
  onPageChange: ({ loadRecipes, changePage, recipes }) => page => {
    const { currentPage, results, query } = recipes;
    if (currentPage == page) return;
    if (!results[page]) {
      loadRecipes(query, page);
    } else {
      changePage(page);
    }
  },
  changeQuery: ({ loadRecipes }) => e => loadRecipes(e.target.innerHTML),
});

const mapStateToProps = (state) => ({
  recipes: state.recipes,
})
const enhance = compose(
  connect(mapStateToProps, actions),
  handlers,
  withLifecycle,
)(Recipes);

export default enhance;
