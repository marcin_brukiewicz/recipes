import React from 'react';
import { compose, withState, withHandlers } from 'recompose';
import { Select } from 'antd';

const SearchBox = ({ onClick, onChange, query }) => (
  <div className="search-box">
    <Select
      mode="tags"
      style={{ width: '100%' }}
      value={query ? query.split(',') : []}
      onChange={onChange}
      tokenSeparators={[',']}
      notFoundContent={false}
      filterOption={false}
    >
    </Select>
    <button onClick={onClick}>Search</button>
  </div>
);

const enhance = compose(
  withHandlers({
    onChange: ({ search }) => (value) => {
      search(value.join(','));
    },
    onClick: ({ search, query }) => (e) => {
      search(query);
    },
  }),
)(SearchBox);

export default enhance;
