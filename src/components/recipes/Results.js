import React from 'react';
import { Table } from 'antd'
import { compose, branch, renderComponent, withHandlers } from 'recompose';
import { DoubleBounce } from 'better-react-spinkit';
import { Pagination } from 'antd';

const Ingredient = ({ changeQuery, ingredient }) => (
  <li
    className="recipe--ingredient"
    onClick={changeQuery}
  >
    {ingredient}
  </li>
);

const htmlDecode = input => (new DOMParser().parseFromString(input, "text/html")).documentElement.textContent;
const Recipe = ({ href, thumbnail, title, ingredients, changeQuery }) => (
  <div className="recipe">
    <div className="img" style={{backgroundImage: `url('${thumbnail}')`}}/>
    <div className="info">
      <a className="title" href={href} target="_blank">{htmlDecode(title)}</a>
      <ul>
        {ingredients.split(', ').map((ingredient, i) => <Ingredient key={i} ingredient={ingredient} changeQuery={changeQuery} />)}
      </ul>
    </div>
  </div>
);

const Results = ({ currentPage, results, changePage, changeQuery }) => (
  <div className="results">
    <div className="recipes">
      {results.map((result, i) => <Recipe {...result} key={i} changeQuery={changeQuery} />)}
    </div>
    <Pagination onChange={changePage} total={1000} current={currentPage} />
  </div>
);

const Placeholder = props => (
  <div className="placeholder">
    {props.children}
  </div>
);

const noResults = branch(
  ({ results, loading, query }) => results.length === 0 && !loading && query,
  renderComponent(props => (
    <Placeholder>
      <span>There is nothing here :(</span>
    </Placeholder>
  )),
);

const blank = branch(
  ({ query}) => !query,
  renderComponent(props => <div></div>),
);

const isLoading = branch(
  ({ loading }) => loading,
  renderComponent(props => (
    <Placeholder>
      <DoubleBounce size={50} />
    </Placeholder>
  )),
);

const enhance = compose(
  blank,
  noResults,
  isLoading,
)(Results);


export default enhance;
