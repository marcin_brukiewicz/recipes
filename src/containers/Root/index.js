import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory'
import { Provider } from 'react-redux';
import Recipes from 'Components/Recipes';
import createStore from 'Redux/createStore';
import style from './style.scss'

const history = createBrowserHistory();
const store = createStore(history);

const Root = (props) => (
  <Router history={history}>
    <Provider store={store}>
      <Route path="/" component={Recipes} />
    </Provider>
  </Router>
);




export default Root;
