# Recipes

### Developer mode

1. Install using `yarn`
2. `yarn run dev`
3. Go to `http://localhost:4000`

### Used packages
1. Recompose
2. AntUI
3. react-router v4
4. redux with redux-pack and redux-thunk
5. better-react-spinkit
6. ...
